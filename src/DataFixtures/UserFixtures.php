<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Filesystem\Filesystem;
use App\Service\UploadService;
use Faker;

class UserFixtures extends Fixture
{
    private $encoder;
    public const USER_REFERENCE = 'user';
    private $uploader;
    private $filesystem;
    private $faker;

    public function __construct(UserPasswordEncoderInterface $encoder, Filesystem $filesystem, UploadService $uploader)
    {
        $this->encoder = $encoder;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        $this->faker = Faker\Factory::create('en_EN');
    }

    public function load(ObjectManager $manager)
    {
        //On se crée 9 users classiques
        for ($x = 1; $x < 8; $x++) {
            $user = new User();
            $user->setUsername($this->faker->name);
            $user->setPassword($this->encoder->encodePassword($user, '12345'));
            $user->setRole('ROLE_USER');
            $manager->persist($user);
            //On ajoute chaque user en référence pour d'autres fixtures
            $this->addReference(self::USER_REFERENCE.$x, $user);
        }
        //On se crée un user admin
        $user = new User();
        $user->setUsername('admin');
        $user->setPassword($this->encoder->encodePassword($user, 'admin'));
        $user->setRole('ROLE_ADMIN');
        $manager->persist($user);

        $manager->flush();
    }
}
