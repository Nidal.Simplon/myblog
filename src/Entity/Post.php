<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * La propriété fileImage nous servira uniquement lors du formulaire
     * pour stocker le fichier image avant sont upload/renommage, elle n'a
     * pas d'annotation ORM car elle n'est pas destinée à persister en base
     * de données, ce qu'on stock dans la base c'est juste le nom de l'image
     */

    /**
     * @Assert\Image(maxSize="5M")
     */
    private $fileImage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getImage(): ?string
    {
        /**
         * Ici quand on lui dit de nous renvoyer le nom de l'image, on
         * la concatène avec l'URL de là où on upload nos images sur
         * le serveur
         * Honnêtement je suis pas ultra satisfait de cette partie et je
         * me demande si ça posera pas de soucis quelque part, je l'ai fait
         * pasque j'arrivais pas à accéder à mes variables d'environnements
         * directement depuis le twig
         */
        return $_ENV['UPLOAD_URL'] . $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getFileImage()
    {
        return $this->fileImage;
    }

    public function setFileImage($fileImage): self
    {
        $this->fileImage = $fileImage;

        return $this;
    }
}
