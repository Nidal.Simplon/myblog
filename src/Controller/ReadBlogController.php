<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Repository\PostRepository;
use App\Entity\Post;

class ReadBlogController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(UserRepository $repo)
    {

        return $this->render('read_blog/index.html.twig', [
            'userList' => $repo->findAll(),
        ]);
    }
    /**
     * @Route("/blog/{id}", name="one_blog")
     */
    public function oneBlog(User $user, PostRepository $repo) {
        //$posts = $repo->findBy(['author' => $user]);
        
        return $this->render('read_blog/one-blog.html.twig', [
            'user' => $user
        ]);

    }
     /**
     * @Route("/post/{id}", name="one_post")
     */
    public function onePost(Post $post) {
        
        return $this->render('read_blog/one-post.html.twig', [
            'post' => $post
        ]);

    }
}
